int currRead = 0;
String readCommand = "";
String readVal = "";

void setup() {
  Serial.begin(9600);
  initEyes();
  initMouth();
  initMeter();
}

void loop() {
  if (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar == 10) {
    } else if (inChar == '?') {
      if (readCommand == "meter") {
        setMeter(readVal.toInt());
      } else if (readCommand == "eye_left") {
        setEyes("left", readVal);
      } else if (readCommand == "eye_right") {
        setEyes("right", readVal);
      } else if (readCommand == "mouth_speak") {
        mouthSpeak(readVal);
      } else if (readCommand == "mouth_scramble") {
        mouthScramble(readVal);
      }
      readCommand = "";
      readVal = "";
      currRead = 0;
    } else if (inChar == ':') {
      currRead = 1;
    } else {
      if (currRead == 0) {
        readCommand += inChar;
      } else {
        readVal += inChar;
      }
    }
  }
  updateMouth();
}




// ------------------
//        METER
// ------------------
#include <Servo.h>
Servo meterServo;

void initMeter() {
  meterServo.attach(9);
}

void setMeter(int val) {
  Serial.print("set meter - ");
  Serial.println(val);
  val = map(val, 0, 100, 0, 85);
  meterServo.write(val);
}



// -------------------
//      EYES
// -------------------
#include <Adafruit_NeoPixel.h>
#define EYES_PIN        10
#define EYE_LEFT_PIXELS 12
#define EYE_RIGHT_PIXELS 6

Adafruit_NeoPixel eyePixels(EYE_LEFT_PIXELS + EYE_RIGHT_PIXELS, EYES_PIN, NEO_GRB + NEO_KHZ800);

void initEyes() {
  eyePixels.begin();
}

void setEyes(String eyeName, String colorStr) {
  int rgb[] = stringToRGB(val);

  if (eyeName == "left") {
    for (int i = 0; i < EYE_LEFT_PIXELS; i++) {
      eyePixels.setPixelColor(i, eyePixels.Color(rgb[0],rgb[1],rgb[2]));
    }
  } else {
    for (int i = 0; i < EYE_RIGHT_PIXELS; i++) {
      eyePixels.setPixelColor(i + EYE_LEFT_PIXELS, eyePixels.Color());
    }
  }
  eyePixels.show();
}


// -------------------
//      MOUTH
// -------------------
#define MOUTH_PIN        3
#define MOUTH_PIXELS     52
#define MOUTH_ACTION_IDLE       0
#define MOUTH_ACTION_SPEAK      1
#define MOUTH_ACTION_SCRAMBLE   2

Adafruit_NeoPixel mouthPixels(MOUTH_PIXELS, MOUTH_PIN, NEO_GRB + NEO_KHZ800);

void initMouth() {
  mouthPixels.begin();
}

uint32_t mouthColor;
int mouthAction = MOUTH_IDLE;
int mouthState = 0;

void mouthSpeak(String val) {
  int rgb[] = stringToRGB(val);
  mouthColor = mouthPixels.Color(rgb[0],rgb[1],rgb[2]);
  mouthAction = MOUTH_ACTION_SPEAK;
  mouthState = 26;
}

void mouthScramble(Stirng val){
    int rgb[] = stringToRGB(val);
    mouthColor = mouthPixels.Color(rgb[0],rgb[1],rgb[2]);
    mouthAction = MOUTH_ACTION_SCRAMBLE;
}

void updateMouth(){
    if (mouthAction == MOUTH_ACTION_SPEAK){
        if (mouthState > 0){
            mouthPixels.clear();
            mouthState -= 1;
            for (int i=0;i<mouthState;i++){
                mouthPixels.setPixelColor(26 + i, mouthColor);
                mouthPixels.setPixelColor(26 - i, mouthColor);
            }
            mouthPixels.show();
            delay(18);
        } else {
            mouthAction = MOUTH_ACTION_IDLE;
            mouthPixels.clear();
        }
    } else if (mouthAction == MOUTH_ACTION_SCRAMBLE){
        if (mouthState > 0){
            mouthPixels.clear();
            mouthState -= 1;
            for (int i=0;i<mouthState;i++){
                int r = floor(random(0,MOUTH_PIXELS-1));
                mouthPixels.setPixelColor(r, mouthColor);
            }
            mouthPixels.show();
            delay(18);
        } else {
            mouthAction = MOUTH_ACTION_IDLE;
            mouthPixels.clear();
        }
    }
}

int stringToRGB(String val){
  String vals[] = {"", "", ""};
  int valsIndex = 0;
  for (int i = 0; i < val.length(); i++) {
    char c = val.charAt(i);
    if (c == ',') {
      valsIndex++;
    } else {
      vals[valsIndex] += c;
    }
  }
  return {vals[0].toInt(),vals[1].toInt(),vals[2].toInt()};
}