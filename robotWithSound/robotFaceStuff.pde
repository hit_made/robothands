Serial myPort;
int meterVal;
void initFace(){
  printArray(Serial.list());
  String portName = Serial.list()[1];
  myPort = new Serial(this, portName, 9600);
}

//--------------------
// ---- EYES ---------
// -------------------
void sendEyes(color c){
  sendEyes(c,c);
}
void sendEyes(color cr, color cl){
  String s = "eye_right:"+int(red(cr))+","+int(green(cr))+","+int(blue(cr))+"?";
  sendToArduino(s);
  s = "eye_left:"+int(red(cl))+","+int(green(cl))+","+int(blue(cl))+"?";
  sendToArduino(s);
}

// ------------------
// ----- METER ------
// ------------------
void meterDown(){
  sendMeter(meterVal-2);
}
void meterUp(){
  sendMeter(meterVal+2);
}
void sendMeter(int val){
  val = constrain(val,0,100);
  meterVal = val;
  sendToArduino("meter:"+val+"?");
}

// --------------------
// ------ MOUTH -------
// --------------------
void sendMouth(color c, String mouthType){
  sendToArduino("mouth_"+mouthType+":"+int(red(c))+","+int(green(c))+","+int(blue(c))+"?");
}

// --------------------
// ------ OTHER -------
// --------------------
void sendToArduino(String s){
  println(s);
  myPort.write(s);
}

void exit(){
  myPort.write("eye_left:0,0,0?");
  myPort.write("eye_right:0,0,0?");
}
