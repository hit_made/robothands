import processing.serial.*;

void setup()  {
  size(300, 200);
  initFace();
  initSounds();
  stroke(255);
}

void draw() {
  background(0);
  for (int i=50;i<width;i+=50){
    line(i,0,i,height);
  }
  updateSound();
}

void mousePressed(){
  int i = floor(mouseX/50);
  playSound(fileNames[i]);
}
