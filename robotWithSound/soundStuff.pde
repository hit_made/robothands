import ddf.minim.*;

Minim minim;
AudioSample[] sounds;

color[] colors = {color(255,0,0),
                  color(0,255,0),
                  color(0,0,255),
                  color(255,255,0),
                  color(255,0,255),
                  color(0,255,255)};
String[] mouthTypes =  {"scramble","speak","speak","speak","scramble","scramble"};
String[] fileNames = {"calculating","cute","oops","singing","sound1","sound2"};

void initSounds(){
  minim = new Minim(this);
  sounds = new AudioSample[fileNames.length];
  for (int i=0;i<fileNames.length;i++){
    sounds[i] = minim.loadSample( fileNames[i]+".mp3");
  }
}

void playSound(String s){
  int i = java.util.Arrays.asList(fileNames).indexOf(s);
  sounds[i].trigger();
}

void updateSound(){
  for (int i=0;i<sounds.length;i++){
    for (int j=0;j<sounds[i].bufferSize()-1;j+=10){
      if (sounds[i].mix.get(j)> 0.2){
        sendMouth(colors[i],mouthTypes[i]);
      }
    }
  }
}
