import processing.serial.*;

Serial myPort;
int val;
color c1,c2;

void setup()  {
  size(200, 200);
  printArray(Serial.list());
  String portName = Serial.list()[1];
  myPort = new Serial(this, portName, 9600);
  setGradient(0,height*0.7,width,height*0.3,randomColor(),randomColor());
  noStroke();
  c1 = randomColor();
  fill(c1);
  rect(0,0,width/2,height/2);
  c2 = randomColor();
  fill(c2);
  rect(width/2,0,width/2,height/2);
}

color randomColor(){
  return color(random(255),random(255),random(255));
}

void setGradient(int x, float y, float w, float h, color c1, color c2 ) {
  noFill();
  for (int i = x; i <= x+w; i++) {
    float inter = map(i, x, x+w, 0, 1);
    color c = lerpColor(c1, c2, inter);
    stroke(c);
    line(i, y, i, y+h);
  }
}

void draw() {
}

void mousePressed(){
  if (mouseY<height/2){
    if (mouseX>width/2){
      c2 = randomColor();
      String s  = "eye_right:"+int(red(c2))+","+int(green(c2))+","+int(blue(c2))+"?";
      sendToArduino(s);
      fill(c2);
      rect(width/2,0,width/2,height/2);
    } else {
      c1 = randomColor();
      String s  = "eye_left:"+int(red(c1))+","+int(green(c1))+","+int(blue(c1))+"?";
      sendToArduino(s);
      fill(c1);
      rect(0,0,width/2,height/2);
    }
  } else if (mouseY<height*0.7){
      sendToArduino("mouth:255,0,0?");
  } else {
    int val = int(map(mouseX,0,width,0,100));
    sendToArduino("meter:"+val+"?");
  }
}

void sendToArduino(String s){
  println(s);
  myPort.write(s);
}

void exit(){
  myPort.write("eye_left:0,0,0?");
  myPort.write("eye_right:0,0,0?");
}
